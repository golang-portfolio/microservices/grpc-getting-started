.PHONY: protos

protos:
	protoc -I protos/ protos/routeguide/institutions.proto --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
	protoc -I protos/ protos/routeguide/route_guide.proto --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
